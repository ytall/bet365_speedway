from pprint import pprint
import json
import datetime as dt

from utils import db_utils

TABLENAME = "results"

def search(where = {}, limit = 50, fetch_all = True, fetch_one = False):
    ''' Retornar pesquisa por resultados '''
    
    where_str = ""
    where_arr = []
    
    if where:

        if ('event_time' in where and where['event_time']):
            where_arr.append("event_time = '" + str(where['event_time']) + "'")

        if ('last_24_hours' in where):
            where_arr.append("created_at > (now() - interval '24 hours')")
            
        if (len(where_arr) > 0):
            where_str = ' where ' + (' and '.join(where_arr))
    
    select = [
        "*",
        "to_char(created_at, 'YYYY-MM-DD HH24:MI:SS') as created_at",
    ]

    select_str = ", ".join(select)
    
    query = "select " + select_str + " from " + TABLENAME + where_str
    
    query = query + " order by id desc"

    query = query + " limit " + str(limit)
        
    result = db_utils.execute_query(query, fetch_all = fetch_all, fetch_one = fetch_one)

    return result

def insert(data, conn=None):
    ''' Inserir resultado no banco de dados '''

    insert = data.copy()

    insert['created_at'] = dt.datetime.now()

    inserted_id = None

    check_insert_where = {
        'event_time': data['event_time'],
        'last_24_hours': True
    }

    check_result = search(where = check_insert_where, fetch_one = True)

    if check_result == None:
    
        if (conn):
            inserted_id = db_utils.insert_query_conn(conn, TABLENAME, insert)
            
        else:
            inserted_id = db_utils.insert_query(TABLENAME, insert)
        
    return inserted_id